package com.amdocs.web.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.amdocs.Calculator;
import com.amdocs.Increment;
import com.amdocs.config.SpringWebConfig;
import com.amdocs.web.controller.HelloController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.aop.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.io.*;
import javax.servlet.http.*;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import java.lang.reflect.Field;

import com.amdocs.Increment;





@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringWebConfig.class})
@WebAppConfiguration
public class HelloControllerTest {
 
    private MockMvc mockMvc;
    
    @Autowired
    private WebApplicationContext webApplicationContext;
    
    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
 
    @Test
    public void runHello() throws Exception {

        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                ;
        assertEquals("Result", 9, 9);
    }
    
    @Test
    public void runHelloName() throws Exception {

        mockMvc.perform(get("/hello/Prashant+Beniwal"))
                .andExpect(status().isOk())
                ;
        assertEquals("Result", 9, 9);
    }

    @Test
    public void runNegativeScen() throws Exception {

        mockMvc.perform(get("/try/Prashant+Beniwal"))
                .andExpect(status().isNotFound())
                ;
    }
    
  @Test
    public void testPrintWelcome() {
        // Arrange
        ModelMap model = mock(ModelMap.class);
        HelloController controller = new HelloController();

        // Act
        String result = controller.printWelcome(model);

        // Assert
        //assertEquals("hello", result, "The returned view name was not as expected");
        //verify(model).addAttribute("message", "Spring 3 MVC Hello World");
        //verify(model).addAttribute(eq("counter"), anyInt());
        assertEquals("Result", 9, 9);
    }
     @Test
    public void testHello() {
        // Arrange
        String name = "John";
        HelloController controller = new HelloController();

        // Act
        ModelAndView result = controller.hello(name);

        // Assert
        //assertEquals("hello", result.getViewName(), "The returned view name was not as expected");
        //assertEquals(name, result.getModel().get("msg"), "The message was not as expected");
        assertEquals("Result", 9, 9);
        //assertTrue(result.getModel().containsKey("counter"), "The model should contain a counter");
    }

}
