import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.amdocs.Calculator;
import com.amdocs.Increment;
import com.amdocs.config.SpringWebConfig;
import com.amdocs.web.controller.HelloController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.aop.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.io.*;
import javax.servlet.http.*;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import java.lang.reflect.Field;

import com.amdocs.Increment;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import org.apache.commons.io.FileUtils;
public class IncrementTest {

    @Test
    public void testIncrementConstructor() {
        // Act
        Increment increment = new Increment();

        // Assert
        assertNotNull(increment);
    }

    @Test
    public void testGetCounter() {
        // Arrange
        Increment increment = new Increment();

        // Act
        int result = increment.getCounter();

        // Assert
        assertEquals(1, result);
    }

    @Test
    public void testDecreaseCounter() {
        // Arrange
        Increment increment = new Increment();

        // Act
        int resultZero = increment.decreasecounter(0);
        int resultOne = increment.decreasecounter(1);
        int resultOther = increment.decreasecounter(2);

        // Assert
        assertEquals(1, resultZero);
        assertEquals(0, resultOne);
        //assertEquals(1, resultOther);
    }
}